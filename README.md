# Sample Springboot Application
## _CICD Sample Implementation using DevOps Tools_
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This application is a very simple springboot application written in java which displays a simple "_Greetings from Spring Boot!_" as console output when run

This application makes use of a kubernetes cluster with 1 master node and 1 slave node in created in AWS using KOPS. It also uses helm for kubernetes deployment to various environment.

## Features

The .gitlab-ci.yaml file does the following when run:

- Does a gradle build and unit test
- Does code coverage analysis
- Does docker build and push
- Does a deployment in AWS kubernetes cluster using helm chart in dev and prod

## Tools used

| Tool Name | Function |
| ------ | ------ |
| GitLab CI | CICD tool |
| Gradle | compilation and unit testing |
| Sonarcloud | code coverage and code analysis |
| Docker | containerize the application and creates an image |
| Docker Hub | docker image repository |
| AWS | hosts the application in cloud |
| Kubernetes | Deploy the image in cloud |
| Helm | Image (Package Manager) to orchestrate the k8s deployment |

## Building the application

### CICD Steps

The CICD steps are as follows:

![cicd](docs/images/cicd.png)

There are 5 stages in total. Function of each step is shown below:

### Build job:

The build job does a gradle build and unit test and marks the job as successful when there are no compilation or unit test issues. 

The gradle wrapper acts as the gradle run time environment.

![BuildJob](docs/images/BuildJob.png)

### SonarQube job:

This job does a quality check by analyzing the source code by using the code coverage tool, the sonarcloud.

![SonarCloudJob](docs/images/SonarCloudJob.png)

### Docker build and push job:

This job does creates a docker image and necessary tags for the image and pushes the image to the docker hub.

![DockerBuildPushJob](docs/images/DockerBuildPushJob.png)

### Deploy to Dev environment job:

This job deploys the docker image to the dev namespece of the k8s cluster in AWS using helm.

The kubernetes deployment file will be passed from helm templates folder. The values for the deployment and service will be passed during run time using values_dev.yaml file in helm folder.

![DeployToDevJob](docs/images/DeployToDevJob.png)

### Deploy to Prod environment job:

This job deploys the docker image to the prod namespece of the k8s cluster in AWS using helm.

_This deployment job executes only for the main branch as part of release strategy_

The kubernetes deployment file will be passed from helm templates folder. The values for the deployment and service will be passed during run time using values_prod.yaml file in helm folder.

![DeployToProdJob](docs/images/DeployToProdJob.png)

## Branching and Merging Strategy

### Branching Strategy

- The "master" branch is the main branch.
- feature/<featureName> branches will be created from main branch to add new features as part of the new release. This branch will act as integration/qa testing branch.
- hotfix or test branches will be created from the feature branch for testing any new integrations or changes

![BranchingStrategy](docs/images/BranchingStrategy.png)

### Merging Strategy

Below are the screenshots that will explain the merging strategy followed here.

- Select the source and destination branch to create the merge request.

![MergeRequest](docs/images/MergeRequest.png)

- Enter the approver details for approving the PR

![MergeRequestCreation](docs/images/MergeRequestP2.png)

- Approver will review, update the merge request with details if needed.

- GitlabCI will trigger a pipeline for merge request to make sure the to be merged code is building succesfully in the target branch (main branch in this case) and once the pipeline is succesful, the branch will be merged.

- Both steps can be configured to work together for additional security, visibility, and as a best DevOps practice.

![ApproveMergeRequest](docs/images/ApproveMergeRequest.png)

## Validating the Deployment in K8s cluster

### Dev Deployment Validation

- viewing all the deployed components

![GetAllDev](docs/images/GetAllDev.png)

- Deployment info

![](2022-07-03-17-51-07.png)

![GetDeploymentDev](docs/images/GetDeploymentDev.png)

- Service info

![GetServiceDev](docs/images/GetServiceDev.png)

- Accessing URL in the browser

The application can be accessed by the AWS ELB url that is created automatically and appending port 8081 to it.

![AccessAppInDev](docs/images/AccessAppInDev.png)

### PROD Deployment Validation

- viewing all the deployed components

![GetAllProd](docs/images/GetAllProd.png)

- Deployment info

![GetDeploymentProd](docs/images/GetDeploymentProd.png)

- Service info

![GetServiceProd](docs/images/GetServiceProd.png)

- Accessing URL in the browser

The application can be accessed by the AWS ELB url that is created automatically and appending port 8081 to it.

![AccessAppInProd](docs/images/AccessAppInProd.png)

## Steps to create a k8s cluster in AWS

Here the idea is to use EC2 instances to spin up a production like kubernetes cluster to avoid incurring costs as much as possible.

The steps to install the dependencies are as follows:
1. Spin up a t2.micro EC2 instance in AWS and login to that EC2 instance using the pem key

2. create an IAM group called "kops" which the below policies.
```sh
AmazonEC2FullAccess
AmazonRoute53FullAccess
AmazonS3FullAccess
IAMFullAccess
AmazonVPCFullAccess
AmazonSQSFullAccess
AmazonEventBridgeFullAccess
```

3. create an IAM user "kops" under the group "kops" and copy the access key id and the secret access value. (This is required to login to the access the kubernetes cluster which we will be installing via KOPS)

![kopsUser](docs/images/kopsUser.png)

4. export the access key id and secret access key in the ec2 instance as an environment variable
```sh
export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id)
export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key)
```

5. Create a s3 bucket to store the cluster state configurations
s3 bucket name created: sample-springboot-demo-clickatell-devops

![s3Bucket](docs/images/s3Bucket.png)

6. Install Kops and kubectl utility tool
```sh
curl -Lo kops https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
chmod +x ./kops
sudo mv ./kops /usr/local/bin/
curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```

7. Export environment variables for the cluster creation:
```sh
export NAME=samplespringboot.k8s.local
export KOPS_STATE_STORE=s3://sample-springboot-demo-clickatell-devops
```

8. create the kubernetes cluster
```sh
kops create cluster \
    --name=${NAME} \
    --cloud=aws \
    --zones=us-east-1a \
    --discovery-store=s3://sample-springboot-demo-clickatell-devops/${NAME}/discovery 
```

8. It takes around 10 minutes for the cluster to be created. Validate if the cluster is ready by using the following commands
```sh
kops validate cluster
```

![EC2Instances](docs/images/EC2Instances.png)

9. Install helm
```sh
curl -Lo helm https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz
tar -zxvf helm
chmod +x -R ./linux-amd64
sudo mv linux-amd64/helm /usr/local/bin/helm
```

10. create two kuernetes namespace dev and prod
```sh
kubectl create namespace dev
kubectl create namespace prod
```

11. After testing the application, delete cluster to avoid incurring costs. This will also delete the corresponding ec2 instances that we created while setting up the k8s cluster.
```sh
kops delete cluster samplespringboot.k8s.local --yes
```

